package umrtnost.funkce;

import java.util.Map;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;

/**
 * Abstraktni funkce definujici ramec pro prubehy umrti ve souvislosti s opatrenimi.
 * 
 * @author Vladislav Zidek
 *
 */
public abstract class Funkce {
	protected double[] gradient;	// Vektor gradientu
	public double[] parametry;		// Vektor parametru
	
	/**
	 * Vypocet gradientu
	 * 
	 * @param p Vektor parametru
	 * @param td Vstupni data
	 * @return Vypocteny gradient
	 */
	public abstract double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td);
	
	/**
	 * Vypocet vysledku daneho kroku
	 * 
	 * @param vd Vstupni data
	 * @param p Vektor parametru
	 * @return Vypoctena hodnota
	 */
	public abstract Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p);
	
	/**
	 * Unitarni operace minus
	 * 
	 * @param x Vstup
	 * @return Opacna hodnota
	 */
	public final double[] minus(double[] x) {
		for (int i = 0; i < x.length; i++) {
			x[i] = - x[i];
		}
		return x;
	}
}
