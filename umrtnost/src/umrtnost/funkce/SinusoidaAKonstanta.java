package umrtnost.funkce;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;

public class SinusoidaAKonstanta extends Funkce {
	public SinusoidaAKonstanta() {
		parametry = new double[5];
		gradient = new double[5];
	}
	
	@Override
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double c = p[4];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			int t = tyden.getKey();
			double pom = p0 + p1*Math.sin(p2+p3*t)+p1*Math.cos(p2+p3*t);
			double r = c*u + pom - n;
			retVal.put(t, new Vysledek(c*u, pom, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double c = p[4];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		double g2Sum = 0.0f;
		double g3Sum = 0.0f;
		double g4Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> a : td.entrySet()) {
			TydenniData b = a.getValue();
			int u = b.getCovidUmrtnost();
			double n = b.getNadumrtnost();
			int t = a.getKey();
			double d = c*u + p0 + p1*Math.sin(p3*t+p2)+p1*Math.cos(p3*t+p2) - n;
			double e = Math.cos(p3*t+p2)-Math.sin(p3*t+p2);
			g0Sum += 2*d;
			g1Sum += 2*(Math.sin(p3*t+p2)+Math.cos(p3*t+p2))*d;
			g2Sum += 2*p1*e*d;
			g3Sum += 2*p1*t*e*d;
			g4Sum += 2*u*d;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		gradient[2] = g2Sum/td.keySet().size();
		gradient[3] = g3Sum/td.keySet().size();
		gradient[4] = g4Sum/td.keySet().size();		
		return gradient;
	}

}
