package umrtnost.funkce;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.Vysledek;
import umrtnost.data.TydenniData;

public class ParabolaPatehoStupne extends Funkce {
	public ParabolaPatehoStupne() {
		parametry = new double[7];
		gradient = new double[7];
	}
	
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double a0 = p[0];
		double a1 = p[1];
		double a2 = p[2];
		double a3 = p[3];
		double a4 = p[4];
		double a5 = p[5];
		double c = p[6];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			int t = tyden.getKey();
			double pom = a0 + a1*t + a2*t*t + a3*t*t*t + a4*t*t*t*t+ a5*t*t*t*t*t;
			double r = c*u + pom - n;
			retVal.put(t, new Vysledek(c*u, pom, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double p4 = p[4];
		double p5 = p[5];
		double c = p[6];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		double g2Sum = 0.0f;
		double g3Sum = 0.0f;
		double g4Sum = 0.0f;
		double g5Sum = 0.0f;
		double g6Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> a : td.entrySet()) {
			TydenniData b = a.getValue();
			int u = b.getCovidUmrtnost();
			double n = b.getNadumrtnost();
			int t = a.getKey();
			double d = (c*u+p5*t*t*t*t*t+p4*t*t*t*t+p3*t*t*t+p2*t*t+p1*t+p0-n);
			g0Sum += 2*d;
			g1Sum += 2*t*d;
			g2Sum += 2*t*t*d;
			g3Sum += 2*t*t*t*d;
			g4Sum += 2*t*t*t*t*d;
			g5Sum += 2*t*t*t*t*t*d;
			g6Sum += 2*u*d;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		gradient[2] = g2Sum/td.keySet().size();
		gradient[3] = g3Sum/td.keySet().size();
		gradient[4] = g4Sum/td.keySet().size();
		gradient[5] = g5Sum/td.keySet().size();
		gradient[6] = g6Sum/td.keySet().size();
		return gradient;
	}

}
