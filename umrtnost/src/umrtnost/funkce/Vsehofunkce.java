package umrtnost.funkce;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;

public class Vsehofunkce extends Funkce {
	public Vsehofunkce() {
		parametry = new double[11];
		gradient = new double[11];
	}
	
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double p4 = p[4];
		double p5 = p[5];
		double p6 = p[6];
		double p7 = p[7];
		double p8 = p[8];
		double p9 = p[9];
		double c = p[10];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			int t = tyden.getKey();
			double pom = p0 + p1*t + p2*t*t + p3*t*t*t + p4*Math.sin(p5+p6*t) + p4*Math.cos(p5+p6*t) + p7*Math.exp(p8+p9*t);
			double r = c*u + pom - n;
			retVal.put(t, new Vysledek(c*u, pom, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double p4 = p[4];
		double p5 = p[5];
		double p6 = p[6];
		double p7 = p[7];
		double p8 = p[8];
		double p9 = p[9];
		double c = p[10];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		double g2Sum = 0.0f;
		double g3Sum = 0.0f;
		double g4Sum = 0.0f;
		double g5Sum = 0.0f;
		double g6Sum = 0.0f;
		double g7Sum = 0.0f;
		double g8Sum = 0.0f;
		double g9Sum = 0.0f;
		double g10Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> a : td.entrySet()) {
			TydenniData b = a.getValue();
			int u = b.getCovidUmrtnost();
			double n = b.getNadumrtnost();
			int t = a.getKey();
			double f = Math.exp(p9*t+p8);
			double g = Math.sin(p6*t+p5)+Math.cos(p6*t+p5);
			double d = c*u + p4*g + p7*f + p3*t*t*t + p2*t*t + p1*t + p0 - n;
			double e = Math.cos(p6*t+p5)-Math.sin(p6*t+p5);
			g0Sum += 2*d;
			g1Sum += 2*t*d;
			g2Sum += 2*t*t*d;
			g3Sum += 2*t*t*t*d;
			g4Sum += 2*g*d;
			g5Sum += 2*p4*e*d;
			g6Sum += 2*p4*e*t*d;
			g7Sum += 2*f*d;
			g8Sum += 2*p7*f*d;
			g9Sum += 2*t*p7*f*d;
			g10Sum += 2*u*d;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		gradient[2] = g2Sum/td.keySet().size();
		gradient[3] = g3Sum/td.keySet().size();
		gradient[4] = g4Sum/td.keySet().size();
		gradient[5] = g5Sum/td.keySet().size();
		gradient[6] = g6Sum/td.keySet().size();
		gradient[7] = g7Sum/td.keySet().size();
		gradient[8] = g8Sum/td.keySet().size();
		gradient[9] = g9Sum/td.keySet().size();
		gradient[10] = g10Sum/td.keySet().size();
		return gradient;
	}

}
