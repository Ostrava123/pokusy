package umrtnost.funkce;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;

public class Primka extends Funkce{
	public Primka() {
	parametry = new double[3];
	gradient = new double[3];
}

public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
	double a0 = p[0];
	double a1 = p[1];
	double c = p[2];
	
	Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
	for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
		TydenniData td = tyden.getValue();
		int u = td.getCovidUmrtnost();
		double n = td.getNadumrtnost();
		int t = tyden.getKey();
		double r = c*u + a0 + a1*t - n;
		retVal.put(t, new Vysledek(c*u, a0 + a1*t, r));
	}
	return retVal;
}

@Override
public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
	double p0 = p[0];
	double p1 = p[1];
	double c = p[2];
	double g0Sum = 0.0f;
	double g1Sum = 0.0f;
	double g2Sum = 0.0f;
	
	for (Entry<Integer, TydenniData> a : td.entrySet()) {
		TydenniData b = a.getValue();
		int u = b.getCovidUmrtnost();
		double n = b.getNadumrtnost();
		int t = a.getKey();
		double d = (c*u+p1*t+p0-n);
		g0Sum += 2*d;
		g1Sum += 2*t*d;
		g2Sum += 2*u*d;
	}
	gradient[0] = g0Sum/td.keySet().size();
	gradient[1] = g1Sum/td.keySet().size();
	gradient[2] = g2Sum/td.keySet().size();

	return gradient;
}


}
