package umrtnost.funkce;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;

public class ExponencialaAKonstanta extends Funkce {
	public ExponencialaAKonstanta() {
		parametry = new double[5];
		gradient = new double[5];
	}
	
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double a0 = p[0];
		double a1 = p[1];
		double a2 = p[2];
		double a3 = p[3];
		double c = p[4];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			int t = tyden.getKey();
			double pom = a0 + a1*Math.exp(a2+a3*t);
			double r = c*u + pom - n;
			retVal.put(t, new Vysledek(c*u, pom, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double c = p[4];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		double g2Sum = 0.0f;
		double g3Sum = 0.0f;
		double g4Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> a : td.entrySet()) {
			TydenniData b = a.getValue();
			int u = b.getCovidUmrtnost();
			double n = b.getNadumrtnost();
			int t = a.getKey();
			double f = Math.exp(p3*t+p2);
			double d = c*u+p1*f+p0-n;
			g0Sum += 2*d;
			g1Sum += 2*f*d;
			g2Sum += 2*p1*f*d;
			g3Sum += 2*p1*t*f*d;
			g4Sum += 2*u*d;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		gradient[2] = g2Sum/td.keySet().size();
		gradient[3] = g3Sum/td.keySet().size();
		gradient[4] = g4Sum/td.keySet().size();
		return gradient;
	}

}
