package umrtnost.funkce;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;

public class Konstanta extends Funkce {
	public Konstanta() {
		parametry = new double[2];
		gradient = new double[2];
	}
	
	@Override
	public final Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double p0 = p[0];
		double c = p[1];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			int t = tyden.getKey();
			double r = c*u + p0 - n;
			retVal.put(t, new Vysledek(c*u, p0, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double c = p[1];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> a : td.entrySet()) {
			int u = a.getValue().getCovidUmrtnost();
			double n = a.getValue().getNadumrtnost();
			double d= (c*u + p0 - n);
			g0Sum += 2*d;
			g1Sum += 2*u*d;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		return gradient;
	}


}
