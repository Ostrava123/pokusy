package umrtnost.funkce.prisnost;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;
import umrtnost.funkce.Funkce;

public class ParabolaSPrisnosti extends Funkce {
	public ParabolaSPrisnosti() {
		parametry = new double[4];
		gradient = new double[4];
	}

	@Override
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double c0 = p[3];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			double s = td.getPrisnost();
			int t = tyden.getKey();
			double pom = (p2*t*t + p1*t + p0)*s;
			double r = c0*u + pom - n;
			retVal.put(t, new Vysledek(c0*u, pom, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double c0 = p[3];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		double g2Sum = 0.0f;
		double g3Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> tyden : td.entrySet()) {
			TydenniData a = tyden.getValue();
			int u = a.getCovidUmrtnost();
			double n = a.getNadumrtnost();
			double s = a.getPrisnost();
			int t = tyden.getKey();
			double pom = c0*u+s*(p2*t*t+p1*t+p0)-n;
			g0Sum += 2*s*pom;
			g1Sum += 2*s*t*pom;
			g2Sum += 2*s*t*t*pom;
			g3Sum += 2*u*pom;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		gradient[2] = g2Sum/td.keySet().size();
		gradient[3] = g3Sum/td.keySet().size();
		return gradient;

	}
}
