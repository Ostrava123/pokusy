package umrtnost.funkce.prisnost;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;
import umrtnost.funkce.Funkce;

public class KonstantaSPrisnosti extends Funkce {
	public KonstantaSPrisnosti() {
		parametry = new double[2];
		gradient = new double[2];
	}

	@Override
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double p0 = p[0];
		double c0 = p[1];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			double s = td.getPrisnost();
			int t = tyden.getKey();
			double r = c0*u + p0*s - n;
			retVal.put(t, new Vysledek(c0*u, p0*s, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double c0 = p[1];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> tyden : td.entrySet()) {
			TydenniData a = tyden.getValue();
			int u = a.getCovidUmrtnost();
			double n = a.getNadumrtnost();
			double s = a.getPrisnost();
			double d= (c0*u + p0*s - n);
			g0Sum += 2*s*d;
			g1Sum += 2*u*d;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		return gradient;

	}
}
