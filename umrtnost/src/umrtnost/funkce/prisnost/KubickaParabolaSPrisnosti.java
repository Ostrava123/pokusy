package umrtnost.funkce.prisnost;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;
import umrtnost.funkce.Funkce;

public class KubickaParabolaSPrisnosti extends Funkce {
	public KubickaParabolaSPrisnosti() {
		parametry = new double[5];
		gradient = new double[5];
	}

	@Override
	public Map<Integer, Vysledek> vycisli(Map<Integer, TydenniData> vd, double[] p) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double c0 = p[4];
		
		Map<Integer, Vysledek> retVal = new LinkedHashMap<Integer, Vysledek>();
		for (Entry<Integer, TydenniData> tyden : vd.entrySet()) {
			TydenniData td = tyden.getValue();
			int u = td.getCovidUmrtnost();
			double n = td.getNadumrtnost();
			double s = td.getPrisnost();
			int t = tyden.getKey();
			double pom = (p3*t*t*t + p2*t*t + p1*t + p0)*s;
			double r = c0*u + pom - n;
			retVal.put(t, new Vysledek(c0*u, pom, r));
		}
		return retVal;
	}

	@Override
	public double[] vypoctiGradient(double[] p, Map<Integer, TydenniData> td) {
		double p0 = p[0];
		double p1 = p[1];
		double p2 = p[2];
		double p3 = p[3];
		double c0 = p[4];
		double g0Sum = 0.0f;
		double g1Sum = 0.0f;
		double g2Sum = 0.0f;
		double g3Sum = 0.0f;
		double g4Sum = 0.0f;
		
		for (Entry<Integer, TydenniData> tyden : td.entrySet()) {
			TydenniData a = tyden.getValue();
			int u = a.getCovidUmrtnost();
			double n = a.getNadumrtnost();
			double s = a.getPrisnost();
			int t = tyden.getKey();
			double pom = 2*(c0*u+s*(p3*t*t*t+p2*t*t+p1*t+p0)-n);
			g0Sum += s*pom;
			g1Sum += s*t*pom;
			g2Sum += s*t*t*pom;
			g3Sum += s*t*t*t*pom;
			g4Sum += u*pom;
		}
		gradient[0] = g0Sum/td.keySet().size();
		gradient[1] = g1Sum/td.keySet().size();
		gradient[2] = g2Sum/td.keySet().size();
		gradient[3] = g3Sum/td.keySet().size();
		gradient[4] = g4Sum/td.keySet().size();
		return gradient;

	}
}
