package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.Primka;

/**
 * Metoda sdruzeneho grandientu pro linearni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuPrimka extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuPrimka() {
		super();
		Funkce k = new Primka();
		init(k, k.parametry.length);
	}
}
