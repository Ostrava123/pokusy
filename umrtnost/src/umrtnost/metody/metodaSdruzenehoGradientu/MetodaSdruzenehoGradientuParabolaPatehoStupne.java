package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.ParabolaPatehoStupne;

/**
 * Metoda sdruzeneho grandientu pro polynom pateho stupne umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuParabolaPatehoStupne extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuParabolaPatehoStupne() {
		super();
		Funkce k = new ParabolaPatehoStupne();
		init(k, k.parametry.length);
	}
}
