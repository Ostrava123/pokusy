package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.KubickaParabola;

/**
 * Metoda sdruzeneho grandientu pro kubickou funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuKubickaParabola extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuKubickaParabola() {
		super();
		Funkce k = new KubickaParabola();
		init(k, k.parametry.length);
	}
}
