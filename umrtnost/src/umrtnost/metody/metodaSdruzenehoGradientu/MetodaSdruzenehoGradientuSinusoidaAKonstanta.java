package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.SinusoidaAKonstanta;

/**
 * Metoda sdruzeneho grandientu pro sinousoidni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuSinusoidaAKonstanta extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuSinusoidaAKonstanta() {
		super();
		Funkce k = new SinusoidaAKonstanta();
		init(k, k.parametry.length);
	}
}
