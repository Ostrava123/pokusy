package umrtnost.metody.metodaSdruzenehoGradientu;

import java.util.Arrays;
import java.util.Map;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;
import umrtnost.funkce.Funkce;
import umrtnost.metody.Metoda;

public abstract class MetodaSdruzenehoGradientu extends Metoda {
	private double[] minulySmerPoklesu = null;
	private double[] minulySdruzenySmerPoklesu = null;
	
	// Inicializace promennych
	protected void init(Funkce f, int pocetNeznamych) {
		funkce = f;
		minulySmerPoklesu = new double[pocetNeznamych];
		minulySdruzenySmerPoklesu = new double[pocetNeznamych];
	}
	
	protected MetodaSdruzenehoGradientu( ) {
		super(null);
	}

	// Hledani bodu, kde je rezidium mensi nez v minulem kroku. 
	// Hleda se postupnym priblizovanim k vysledku minuleho kroku po polopřímce určenou sdruzenym gradientem.
	// Pouzita metoda posana zde: https://en.wikipedia.org/wiki/Nonlinear_conjugate_gradient_method
	@Override
	public double[] dalsiKrok(Map<Integer, TydenniData> td, double[] p) {
		// Vypocet smeru poklesu v bode minuleho kroku
		double[] smerPoklesu = funkce.minus(funkce.vypoctiGradient(p, td));
		
		// Vypocet smeru sdruzeneho smeru poklesu
		double beta = beta(smerPoklesu, minulySmerPoklesu);
		double[] sdruzenySmerPoklesu = sdruzenySmerPoklesu(minulySdruzenySmerPoklesu, smerPoklesu, beta);
		
		// Vypocet vystupu a rezidia dle dat z minuleho kroku.
		Map<Integer, Vysledek> v = funkce.vycisli(td, p);
		double[] r = vypoctiRezidium(v);
		
		// Inicializace pomocnych promennych
		double[] pPom = new double[p.length];
		double gamma = 1;
		int j = 0;
		
		// Hledani bodu, kde je rezidium mensi nez v minulem kroku. 
		// Hleda se postupnym priblizovanim k vysledku minuleho kroku po polopřímce určenou sdruzenym smerem poklesu.
		for (; j < 65; j++) {
			for (int i = 0; i < p.length; i++) {
				pPom[i] = p[i] + sdruzenySmerPoklesu[i]/gamma;
			}
			Map<Integer, Vysledek> vPom = funkce.vycisli(td, pPom);
			double[] rPom = vypoctiRezidium(vPom);
			
			// Norma neni konecne cislo ve smyslu Java Double, musime ukoncit vypocet.
			if (!Double.isFinite(norma(r)) || norma(rPom) < norma(r)) {
				if (!Double.isFinite(norma(r))) {
					jeMinimum = true;
				}
				break;
			}
			gamma *= 2;
			
			// Preteceni, musime ukoncit vypocet.
			// Zmena gradientu je jiz nespocitatelny pomoci Java double.
			if (!Double.isFinite(gamma)) {
				jeMinimum = true;
			}
		}
		minulySdruzenySmerPoklesu = Arrays.copyOf(sdruzenySmerPoklesu, sdruzenySmerPoklesu.length);
		minulySmerPoklesu = Arrays.copyOf(smerPoklesu, smerPoklesu.length);
		return pPom;
	}

	/**
	 * Vypocet sdruzeneho smeru poklesu.
	 * 
	 * @param mssp Minuly sdruzeny smer poklesu
	 * @param sp Novy tradicni smer poklesu
	 * @param beta Beta z metody sdruzeneho gradientu
	 * @return Novy sdruzeny smer poklesu
	 */
	private double[] sdruzenySmerPoklesu(double[] mssp, double[] sp, double beta) {
		if (mssp.length != sp.length) {
			throw new IllegalArgumentException();
		}
		double[] retVal = new double[mssp.length];
		for (int i = 0; i < retVal.length; i++) {
			retVal[i] = sp[i] + beta * mssp[i]; 
		}
		return retVal;
	}

	/**
	 * Vypocet parametru beta.
	 * Pouziva se maximum z hodnot 0 a beta dle Polaka–Ribiereho.
	 * 
	 * @param sp Novy tradicni smer poklesu
	 * @param msp Minuly tradicni smer poklesu
	 * @return Novy parametr beta
	 */
	private double beta(double[] sp, double[] msp) {
		double jmenovatel = skalarniSoucin(msp, msp);
		if (jmenovatel == 0) {
			return 0;
		}
		double betaPR = skalarniSoucin(sp, rozdil(sp, msp))/jmenovatel;
		return Math.max(betaPR, 0.0);
	}
}
