package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.ExponencialaAKonstanta;
import umrtnost.funkce.Funkce;

/**
 * Metoda sdruzeneho grandientu pro exponecialni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuExponencialaAKonstanta extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuExponencialaAKonstanta() {
		super();
		Funkce k = new ExponencialaAKonstanta();
		init(k, k.parametry.length);
	}
}
