package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.Parabola;

/**
 * Metoda sdruzeneho grandientu pro kvardatickou funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuParabola extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuParabola() {
		super();
		Funkce k = new Parabola();
		init(k, k.parametry.length);
	}
}
