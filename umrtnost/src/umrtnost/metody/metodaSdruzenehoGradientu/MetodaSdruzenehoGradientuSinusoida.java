package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.Sinusoida;

/**
 * Metoda sdruzeneho grandientu pro sinusoidni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuSinusoida extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuSinusoida() {
		super();
		Funkce k = new Sinusoida();
		init(k, k.parametry.length);
	}
}
