package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Exponenciala;
import umrtnost.funkce.Funkce;

/**
 * Metoda sdruzeneho grandientu pro exponecialni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuExponenciala extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuExponenciala() {
		super();
		Funkce k = new Exponenciala();
		init(k, k.parametry.length);
	}
}
