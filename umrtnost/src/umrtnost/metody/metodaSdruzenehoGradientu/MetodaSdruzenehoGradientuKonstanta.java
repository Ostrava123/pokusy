package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Konstanta;

/**
 * Metoda sdruzeneho grandientu pro konstantni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuKonstanta extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuKonstanta() {
		super();
		Konstanta k = new Konstanta();
		init(k, k.parametry.length);
	}
}
