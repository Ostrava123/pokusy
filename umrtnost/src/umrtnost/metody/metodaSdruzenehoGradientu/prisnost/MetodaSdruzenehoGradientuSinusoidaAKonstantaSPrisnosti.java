package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.prisnost.SinusoidaAKonstantaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro sinusoidni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuSinusoidaAKonstantaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuSinusoidaAKonstantaSPrisnosti() {
		super();
		Funkce k = new SinusoidaAKonstantaSPrisnosti();
		init(k, k.parametry.length);
	}
}
