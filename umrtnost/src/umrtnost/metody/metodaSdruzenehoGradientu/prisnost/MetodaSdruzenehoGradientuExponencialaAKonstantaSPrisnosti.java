package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.prisnost.ExponencialaAKonstantaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro exponencialni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuExponencialaAKonstantaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuExponencialaAKonstantaSPrisnosti() {
		super();
		Funkce k = new ExponencialaAKonstantaSPrisnosti();
		init(k, k.parametry.length);
	}
}
