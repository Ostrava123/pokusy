package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.prisnost.ParabolaPatehoStupneSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro polynom pateho stupne umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuParabolaPatehoStupneSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuParabolaPatehoStupneSPrisnosti() {
		super();
		Funkce k = new ParabolaPatehoStupneSPrisnosti();
		init(k, k.parametry.length);
	}
}
