package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.prisnost.SinusoidaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro sinusoidni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuSinusoidaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuSinusoidaSPrisnosti() {
		super();
		Funkce k = new SinusoidaSPrisnosti();
		init(k, k.parametry.length);
	}
}
