package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.prisnost.KubickaParabolaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro kubickou funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuKubickaParabolaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuKubickaParabolaSPrisnosti() {
		super();
		KubickaParabolaSPrisnosti k = new KubickaParabolaSPrisnosti();
		init(k, k.parametry.length);
	}

}
