package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.prisnost.KonstantaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro konstantni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuKonstantaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuKonstantaSPrisnosti() {
		super();
		KonstantaSPrisnosti k = new KonstantaSPrisnosti();
		init(k, k.parametry.length);
	}

}
