package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.prisnost.VsehofunkceSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro obecnou funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuVsehofunkceSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuVsehofunkceSPrisnosti() {
		super();
		Funkce k = new VsehofunkceSPrisnosti();
		init(k, k.parametry.length);
	}
}
