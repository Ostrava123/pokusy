package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.prisnost.PrimkaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro linearni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuPrimkaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuPrimkaSPrisnosti() {
		super();
		PrimkaSPrisnosti k = new PrimkaSPrisnosti();
		init(k, k.parametry.length);
	}

}
