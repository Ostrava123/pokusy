package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.Funkce;
import umrtnost.funkce.prisnost.ExponencialaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro exponencialni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuExponencialaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuExponencialaSPrisnosti() {
		super();
		Funkce k = new ExponencialaSPrisnosti();
		init(k, k.parametry.length);
	}

}
