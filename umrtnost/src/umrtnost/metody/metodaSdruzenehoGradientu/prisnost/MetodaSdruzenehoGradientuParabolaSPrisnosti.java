package umrtnost.metody.metodaSdruzenehoGradientu.prisnost;

import umrtnost.funkce.prisnost.ParabolaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientu;

/**
 * Metoda sdruzeneho grandientu pro kvardatickou funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuParabolaSPrisnosti extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuParabolaSPrisnosti() {
		super();
		ParabolaSPrisnosti k = new ParabolaSPrisnosti();
		init(k, k.parametry.length);
	}

}
