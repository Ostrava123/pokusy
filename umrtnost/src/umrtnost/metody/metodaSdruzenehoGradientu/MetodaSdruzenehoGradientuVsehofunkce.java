package umrtnost.metody.metodaSdruzenehoGradientu;

import umrtnost.funkce.Vsehofunkce;

/**
 * Metoda sdruzeneho grandientu pro sinusoidni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaSdruzenehoGradientuVsehofunkce extends MetodaSdruzenehoGradientu {
	public MetodaSdruzenehoGradientuVsehofunkce() {
		super();
		Vsehofunkce k = new Vsehofunkce();
		init(k, k.parametry.length);
	}
}

