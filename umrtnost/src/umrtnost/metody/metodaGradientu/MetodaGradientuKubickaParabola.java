package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.KubickaParabola;

/**
 * Metoda grandientu pro kubickou funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuKubickaParabola extends MetodaGradientu {
	public MetodaGradientuKubickaParabola() {
		super(new KubickaParabola());
	}
}
