package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.Sinusoida;

/**
 * Metoda grandientu pro sinusoidni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuSinusoida extends MetodaGradientu {
	public MetodaGradientuSinusoida() {
		super(new Sinusoida());
	}
}
