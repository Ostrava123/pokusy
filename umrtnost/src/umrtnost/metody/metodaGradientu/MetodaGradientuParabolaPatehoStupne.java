package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.ParabolaPatehoStupne;

/**
 * Metoda grandientu pro polynom pateho stupne umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuParabolaPatehoStupne extends MetodaGradientu {
	public MetodaGradientuParabolaPatehoStupne() {
		super(new ParabolaPatehoStupne());
	}
}
