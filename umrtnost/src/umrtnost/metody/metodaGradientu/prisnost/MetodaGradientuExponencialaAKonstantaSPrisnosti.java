package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.ExponencialaAKonstantaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro exponencialni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuExponencialaAKonstantaSPrisnosti extends MetodaGradientu {
	public MetodaGradientuExponencialaAKonstantaSPrisnosti() {
		super(new ExponencialaAKonstantaSPrisnosti());
	}
}
