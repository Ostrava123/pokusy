package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.ExponencialaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro exponencialni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuExponencialaSPrisnosti extends MetodaGradientu {

	public MetodaGradientuExponencialaSPrisnosti() {
		super(new ExponencialaSPrisnosti());
	}

}
