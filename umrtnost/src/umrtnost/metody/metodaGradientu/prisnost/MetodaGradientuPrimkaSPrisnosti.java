package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.PrimkaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro linearni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuPrimkaSPrisnosti extends MetodaGradientu {

	public MetodaGradientuPrimkaSPrisnosti() {
		super(new PrimkaSPrisnosti());
	}

}
