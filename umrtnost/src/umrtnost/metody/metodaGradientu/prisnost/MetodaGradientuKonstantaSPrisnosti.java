package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.KonstantaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro konstantni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuKonstantaSPrisnosti extends MetodaGradientu {

	public MetodaGradientuKonstantaSPrisnosti() {
		super(new KonstantaSPrisnosti());
	}

}
