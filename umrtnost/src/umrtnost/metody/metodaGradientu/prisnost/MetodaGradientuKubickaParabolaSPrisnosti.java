package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.KubickaParabolaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro kubickou funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuKubickaParabolaSPrisnosti extends MetodaGradientu {
	public MetodaGradientuKubickaParabolaSPrisnosti() {
		super(new KubickaParabolaSPrisnosti());
	}
}
