package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.ParabolaPatehoStupneSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro polynom pateho stupne umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuParabolaPatehoStupneSPrisnosti extends MetodaGradientu {
	public MetodaGradientuParabolaPatehoStupneSPrisnosti() {
		super(new ParabolaPatehoStupneSPrisnosti());
	}
}
