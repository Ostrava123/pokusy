package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.ParabolaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro kvardatickou funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuParabolaSPrisnosti extends MetodaGradientu {
	public MetodaGradientuParabolaSPrisnosti() {
		super(new ParabolaSPrisnosti());
	}
}
