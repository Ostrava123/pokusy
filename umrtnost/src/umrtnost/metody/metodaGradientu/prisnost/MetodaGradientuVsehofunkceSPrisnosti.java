package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.VsehofunkceSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro obecnou funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuVsehofunkceSPrisnosti extends MetodaGradientu {
	public MetodaGradientuVsehofunkceSPrisnosti() {
		super(new VsehofunkceSPrisnosti());
	}
}
