package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.SinusoidaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro sinusoidni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuSinusoidaSPrisnosti extends MetodaGradientu {
	public MetodaGradientuSinusoidaSPrisnosti() {
		super(new SinusoidaSPrisnosti());
	}
}
