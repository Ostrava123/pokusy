package umrtnost.metody.metodaGradientu.prisnost;

import umrtnost.funkce.prisnost.SinusoidaAKonstantaSPrisnosti;
import umrtnost.metody.metodaGradientu.MetodaGradientu;

/**
 * Metoda grandientu pro sinusoidni funkci umrtnosti na opatreni se zapoctenim prisnosti opatreni
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuSinusoidaAKonstantaSPrisnosti extends MetodaGradientu {
	public MetodaGradientuSinusoidaAKonstantaSPrisnosti() {
		super(new SinusoidaAKonstantaSPrisnosti());
	}
}
