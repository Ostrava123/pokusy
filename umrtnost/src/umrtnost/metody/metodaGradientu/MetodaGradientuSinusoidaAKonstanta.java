package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.SinusoidaAKonstanta;

/**
 * Metoda grandientu pro sinusoidni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuSinusoidaAKonstanta extends MetodaGradientu {

	public MetodaGradientuSinusoidaAKonstanta() {
		super(new SinusoidaAKonstanta());
	}
}
