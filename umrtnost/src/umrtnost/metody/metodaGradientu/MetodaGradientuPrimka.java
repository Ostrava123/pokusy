package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.Primka;

/**
 * Metoda grandientu pro linearni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuPrimka extends MetodaGradientu {
		public MetodaGradientuPrimka() {
			super(new Primka());
		}
}
