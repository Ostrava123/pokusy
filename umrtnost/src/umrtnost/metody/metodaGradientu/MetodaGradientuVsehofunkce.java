package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.Vsehofunkce;

/**
 * Metoda grandientu pro obecnou funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuVsehofunkce extends MetodaGradientu {
	public MetodaGradientuVsehofunkce() {
		super(new Vsehofunkce());
	}
}
