package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.Parabola;

/**
 * Metoda grandientu pro kvardatickou funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuParabola extends MetodaGradientu {
	public MetodaGradientuParabola() {
		super(new Parabola());
	}
}
