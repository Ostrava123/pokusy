package umrtnost.metody.metodaGradientu;

import java.util.Map;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;
import umrtnost.funkce.Funkce;
import umrtnost.metody.Metoda;

public abstract class MetodaGradientu extends Metoda {
	protected MetodaGradientu(Funkce f) {
		super(f);
	}
	
	@SuppressWarnings("unused")
	private MetodaGradientu( ) {
		super(null);
	}

	@Override
	public double[] dalsiKrok(Map<Integer, TydenniData> td, double[] p) {
		// Vypocet gradientu v bode minuleho kroku
		double[] gradient = funkce.vypoctiGradient(p, td);
		
		// Vypocet vystupu a rezidia dle dat z minuleho kroku.
		Map<Integer, Vysledek> v = funkce.vycisli(td, p);
		double[] r = vypoctiRezidium(v);
		
		// Inicializace pomocnych promennych
		double[] pPom = new double[p.length];
		double gamma = 1;
		int j = 0;
		
		// Hledani bodu, kde je rezidium mensi nez v minulem kroku. 
		// Hleda se postupnym priblizovanim k vysledku minuleho kroku po polopřímce určenou gradientem.
		for (; j < 65; j++) {
			for (int i = 0; i < p.length; i++) {
				pPom[i] = p[i] - gradient[i]/gamma;
			}
			Map<Integer, Vysledek> vPom = funkce.vycisli(td, pPom);
			double[] rPom = vypoctiRezidium(vPom);
			
			// Norma neni konecne cislo ve smyslu Java Double, musime ukoncit vypocet.
			if (!Double.isFinite(norma(r)) || norma(rPom) < norma(r)) {
				if (!Double.isFinite(norma(r))) {
					jeMinimum = true;
				}
				break;
			}
			gamma *= 2;
			
			// Preteceni, musime ukoncit vypocet.
			// Zmena gradientu je jiz nespocitatelny pomoci Java double.
			if (!Double.isFinite(gamma)) {
				jeMinimum = true;
			}
		}
		return pPom;
	}
}
