package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.Exponenciala;

/**
 * Metoda grandientu pro exponecialni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuExponenciala extends MetodaGradientu {
	public MetodaGradientuExponenciala() {
		super(new Exponenciala());
	}
}
