package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.Konstanta;

/**
 * Metoda grandientu pro konstantni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuKonstanta extends MetodaGradientu {
	public MetodaGradientuKonstanta() {
		super(new Konstanta());
	}
}
