package umrtnost.metody.metodaGradientu;

import umrtnost.funkce.ExponencialaAKonstanta;

/**
 * Metoda grandientu pro exponencialni funkci umrtnosti na opatreni 
 * 
 * @author Vladislav Zidek
 *
 */
public class MetodaGradientuExponencialaAKonstanta extends MetodaGradientu {
	public MetodaGradientuExponencialaAKonstanta() {
		super(new ExponencialaAKonstanta());
	}
}
