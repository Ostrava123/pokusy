package umrtnost.metody;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.data.Vysledek;
import umrtnost.funkce.Funkce;

public abstract class Metoda {
	protected Map<Integer, Vysledek> vyslednaData;		// Prvek s vysledky, pripadne s mezivysledky
	static int limitCyklu = 3 * 1000   * 1000;			// Maximalni pocet cyklu
	protected boolean jeMinimum = false;				// Informace, zda bylo detekovano minimum
	protected Funkce funkce;							// Vybrana pouzita funkce
	private long cas;									// Delka vypoctu v minisekundach
	
	/**
	 * Konstruktor nastavujici funkci.
	 * @param f
	 */
	protected Metoda(Funkce f) {
		funkce = f;
	}
	
	public Metoda() {
		throw new IllegalArgumentException("Konstruktor musi mit argument!");
	}

	/**
	 * Vypis vystupu do standardniho vystupu
	 */
	public final void vypisVystupy() {
		System.out.println(getClass().getSimpleName() + ":\nParametry: " + Arrays.toString(funkce.parametry));
		if (null != vyslednaData) {
			Vysledek a = sumarizace(vyslednaData);
			System.out.println("Rezidium: " + a.getRezidium());
			System.out.println("Odhad covid: " + a.getOdhadCovid());
			System.out.println("Odhad opatreni: " + a.getOdhadOpatreni());
		}
	}
	
	/**
	 * Secteni dat za cele obdobi
	 * 
	 * @param vd Data za cele obdobi.
	 * @return Sumarizace - odhady jsou secteny, rezidium je soucet absolutnich hodnot.
	 */
	private Vysledek sumarizace(Map<Integer, Vysledek> vd) {
		double c = 0;
		double o = 0;
		double r = 0;
		for (Entry<Integer, Vysledek> a : vd.entrySet()) {
			c += a.getValue().getOdhadCovid();
			o += a.getValue().getOdhadOpatreni();
			r += Math.abs(a.getValue().getRezidium());
		}
		return new Vysledek(c, o, r);
	}
	
	/**
	 * Vypocet vysledku. Inicializace dat, a pak v cyklu pocitani kroku dle zvolene metody a funkce.
	 * 
	 * @param vstupniData Vstupni data zadana v Analyzator.nactiData
	 */
	public final void analyzuj(Map<Integer, TydenniData> vstupniData) {
		System.out.println("zacina " + this.getClass().getSimpleName());
		long zacatek = System.currentTimeMillis();
		funkce.parametry = inicializujParametry(vstupniData, funkce.parametry);
		int i = 0;
		for (; i < limitCyklu; i++) {
			funkce.parametry = dalsiKrok(vstupniData, funkce.parametry);
			if (detekceMinima()) {
				break;
			}
		}
		vyslednaData = funkce.vycisli(vstupniData, funkce.parametry);
		long konec = System.currentTimeMillis();
		cas = (konec-zacatek);
		System.out.println("cyklu " + i + ", cas " + cas);
		System.out.println("konci " + this.getClass().getSimpleName() + " " + new Date().toString());
	}

	/**
	 * Vypocet celkoveho rezidua. Vyuzivano pro detekci konce algoritmu. 
	 * 
	 * @param v
	 * @return Celkove reziduum
	 */
	protected double[] vypoctiRezidium(Map<Integer, Vysledek> v) {
		int size = v.values().size();
		double[] retVal = new double[size];
		int i = 0;
		for (Vysledek d : v.values()) {
			retVal[i] = Math.abs(d.getRezidium());
			i++;
		}
		return retVal;
	}
	
	/**
	 * Inicializace parametru pro zacatek vypoctu. Jako zacatecni stav se bere, ze by byly jen umrti na/s covid(em).
	 * 
	 * @param v Vstupni data
	 * @param p Pole pro inicializaci
	 * @return Inicializovane data
	 */
	public static double[] inicializujParametry(Map<Integer, TydenniData> v, double[] p) {
		Arrays.fill(p, 0.0);
		p[p.length - 1] = 1.0;
		return p;
	}
	
	/**
	 * Dalsi krok vypoctu. Pretizenovana metoda.
	 * @param vd Vstupni data
	 * @param p Pole parametru
	 * @return Nove parametry
	 */
	public abstract double[] dalsiKrok(Map<Integer, TydenniData> vd, double[] p);

	/**
	 * Detekce minima, a tedy konce vypoctu.
	 * 
	 * @return Priznak, zda je konec minima.
	 */
	public final boolean detekceMinima() {
		return jeMinimum ;
	}
	
	/**
	 * Norma vektoru. Pouzita euklidovska.
	 * 
	 * @param vektor Vstupni vektor
	 * @return Norma
	 */
	protected final double norma(double[] vektor) {
		double n = 0.0f;
		
		for (double prvek : vektor) {
			n += prvek*prvek;
		}
		return Math.sqrt(n);
	}
	
	/**
	 * Skalarni soucin dvo vektoru. POuzit bezny, tedy ten, ktery je generovan euklidovskou normou.
	 * 
	 * @param a Prvni vektor
	 * @param b Druhy vektor
	 * @return Skalarni soucin
	 */
	protected double skalarniSoucin(double[] a, double[] b) {
		if (a.length != b.length) {
			throw new IllegalArgumentException();
		}
		double retVal = 0.0;
		for (int i = 0; i < a.length; i++) {
			retVal += a[i]*b[i];
		}
		return retVal;
	}

	/**
	 * Rozdil dvou vektoru
	 * 
	 * @param a Mensitel
	 * @param b Mensenec
	 * @return Rozdil
	 */
	protected double[] rozdil(double[] a, double[] b) {
		if (a.length != b.length) {
			throw new IllegalArgumentException();
		}
		double[] retVal = new double[a.length];
		for (int i = 0; i < a.length; i++) {
			retVal[i] = a[i] - b[i];
		}
		return retVal;
	}

	/**
	 * Zapsani vystupu do CSV souboru.
	 * 
	 * @param fw FileWriter pro zapis do daneho CSV souboru. 
	 * @throws IOException
	 */
	public void zapisVystupy(FileWriter fw) throws IOException {
		Vysledek a = sumarizace(vyslednaData);

		Formatter formatter = new Formatter();;
		String s = formatter.format("%s;%f;%f;%f;%s;%s\n",
				getClass().getSimpleName(), a.getRezidium(), a.getOdhadCovid(), a.getOdhadOpatreni(),
				"" + cas/3600000 + "." + cas/60000 + ":" + cas/1000 + "," + cas%1000, Arrays.toString(funkce.parametry) 
				).toString();
		fw.write(s);

		formatter.close();
	}

}
