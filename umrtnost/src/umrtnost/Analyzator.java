package umrtnost;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Formatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import umrtnost.data.TydenniData;
import umrtnost.metody.Metoda;
import umrtnost.metody.metodaGradientu.MetodaGradientuExponenciala;
import umrtnost.metody.metodaGradientu.MetodaGradientuExponencialaAKonstanta;
import umrtnost.metody.metodaGradientu.MetodaGradientuKonstanta;
import umrtnost.metody.metodaGradientu.MetodaGradientuKubickaParabola;
import umrtnost.metody.metodaGradientu.MetodaGradientuParabola;
import umrtnost.metody.metodaGradientu.MetodaGradientuParabolaPatehoStupne;
import umrtnost.metody.metodaGradientu.MetodaGradientuPrimka;
import umrtnost.metody.metodaGradientu.MetodaGradientuSinusoida;
import umrtnost.metody.metodaGradientu.MetodaGradientuSinusoidaAKonstanta;
import umrtnost.metody.metodaGradientu.MetodaGradientuVsehofunkce;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuExponencialaAKonstantaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuExponencialaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuKonstantaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuParabolaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuPrimkaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuSinusoidaAKonstantaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuSinusoidaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuVsehofunkceSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuKubickaParabolaSPrisnosti;
import umrtnost.metody.metodaGradientu.prisnost.MetodaGradientuParabolaPatehoStupneSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuExponenciala;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuExponencialaAKonstanta;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuKonstanta;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuKubickaParabola;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuParabola;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuParabolaPatehoStupne;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuPrimka;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuSinusoida;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuSinusoidaAKonstanta;
import umrtnost.metody.metodaSdruzenehoGradientu.MetodaSdruzenehoGradientuVsehofunkce;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuExponencialaAKonstantaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuExponencialaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuKonstantaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuKubickaParabolaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuParabolaPatehoStupneSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuParabolaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuPrimkaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuSinusoidaAKonstantaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuSinusoidaSPrisnosti;
import umrtnost.metody.metodaSdruzenehoGradientu.prisnost.MetodaSdruzenehoGradientuVsehofunkceSPrisnosti;

/**
 * 
 * @author Vladislav Zidek
 *
 * Hlavni trida. Vstupni bod a misto pro zadavani dat.
 *
 */
public class Analyzator {
	private static Map<Integer, TydenniData> vstupniData;
	private static Metoda[] metody;
	
	/**
	 * Inicializace objektu Metoda a jejich postupne spousteni.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		metody = new Metoda[] {
				
				new MetodaGradientuKonstanta(),
				new MetodaGradientuPrimka(),
				new MetodaGradientuParabola(),
				new MetodaGradientuKubickaParabola(),
				new MetodaGradientuExponenciala(),
				new MetodaGradientuExponencialaAKonstanta(),
				new MetodaGradientuSinusoida(),
				new MetodaGradientuSinusoidaAKonstanta(),
				new MetodaGradientuVsehofunkce(),
				new MetodaGradientuParabolaPatehoStupne(),
				
				new MetodaSdruzenehoGradientuKonstanta(),
				new MetodaSdruzenehoGradientuPrimka(),
				new MetodaSdruzenehoGradientuParabola(),
				new MetodaSdruzenehoGradientuKubickaParabola(),
				new MetodaSdruzenehoGradientuExponenciala(),
				new MetodaSdruzenehoGradientuExponencialaAKonstanta(),
				new MetodaSdruzenehoGradientuSinusoida(),
				new MetodaSdruzenehoGradientuSinusoidaAKonstanta(),
				new MetodaSdruzenehoGradientuVsehofunkce(),
				new MetodaSdruzenehoGradientuParabolaPatehoStupne(),
				
				new MetodaGradientuKonstantaSPrisnosti(),
				new MetodaGradientuPrimkaSPrisnosti(),
				new MetodaGradientuParabolaSPrisnosti(),
				new MetodaGradientuKubickaParabolaSPrisnosti(),
				new MetodaGradientuExponencialaSPrisnosti(),
				new MetodaGradientuExponencialaAKonstantaSPrisnosti(),
				new MetodaGradientuSinusoidaSPrisnosti(),
				new MetodaGradientuSinusoidaAKonstantaSPrisnosti(),
				new MetodaGradientuVsehofunkceSPrisnosti(),
				new MetodaGradientuParabolaPatehoStupneSPrisnosti(),

				new MetodaSdruzenehoGradientuKonstantaSPrisnosti(),
				new MetodaSdruzenehoGradientuPrimkaSPrisnosti(),
				new MetodaSdruzenehoGradientuParabolaSPrisnosti(),
				new MetodaSdruzenehoGradientuKubickaParabolaSPrisnosti(),
				new MetodaSdruzenehoGradientuExponencialaSPrisnosti(),
				new MetodaSdruzenehoGradientuExponencialaAKonstantaSPrisnosti(),
				new MetodaSdruzenehoGradientuSinusoidaSPrisnosti(),
				new MetodaSdruzenehoGradientuSinusoidaAKonstantaSPrisnosti(),
				new MetodaSdruzenehoGradientuVsehofunkceSPrisnosti(),
				new MetodaSdruzenehoGradientuParabolaPatehoStupneSPrisnosti(),

		};
		
		nactiData();
		for (Metoda m : metody) {
			m.analyzuj(vstupniData);
		}		
		vypisVysledky();
		zapisVysledky();
	}

	/**
	 * Zapsani dat do dvou CSV souboru.
	 */
	private static void zapisVysledky() {
		try {
			zapisVstupy();
			String jmenoSouboru = "vystupy.csv";
			File f = new File(jmenoSouboru);
			f.createNewFile();
			
			FileWriter fw = new FileWriter(jmenoSouboru);
			fw.write("Metoda a Prubeh;Rezidium;Zemreli na/s covid(em);Zemreli v souvislosi s opatrenimi;Cas vypoctu;Parametry\n");
			for (Metoda m : metody) {
				m.zapisVystupy(fw);
			}
			fw.close();
			System.out.println("Vytvoren soubor " + f.getAbsolutePath() + ".");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Zapsani vstupu do CSV souboru
	 * @throws IOException
	 */
	private static void zapisVstupy() throws IOException {
		String jmenoSouboru = "vstupy.csv";
		File f = new File(jmenoSouboru);
		f.createNewFile();
		FileWriter fw = new FileWriter(jmenoSouboru);
		fw.write("Tyden;Zemreli na/s covid(em);Celkova umrtnost;Prumerna umrtnost;Prisnost opatreni\n");
		
		for (Entry<Integer, TydenniData> a : vstupniData.entrySet()) {
			Formatter formatter = new Formatter();
			TydenniData t = a.getValue();
			String s = formatter.format("%d;%d;%d;%f;%f\n",
					a.getKey(), t.getCovidUmrtnost(), t.getAktualniUmrtnost(), t.getPrumernaUmrtnost(), t.getPrisnost()
					).toString();
			fw.write(s);
			formatter.close();
		}

		fw.close();
		System.out.println("Vytvoren soubor " + f.getAbsolutePath() + ".");
	}

	/**
	 * Vypsani dat do standardniho vystupu
	 */
	private static void vypisVysledky() {
		vypisVstupy();
		for (Metoda m : metody) {
			System.out.println();
			m.vypisVystupy();
		}
	}

	/**
	 * Vypsani vstupu do standardniho vystupu
	 */
	private static void vypisVstupy() {
		if (null != vstupniData) {
			vstupniData.forEach((k,a) -> System.out.println(a.toString()));
		}
	}
	
	/**
	 * Nacteni vsech dat po tydnech. Tyden 1 odpovida 39. tydnu 2020.
	 * 
	 * Zdroje dat:
	 * cu - https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/umrti.csv
	 * au, pu - https://www.czso.cz/csu/czso/obypz_cr (au za leta 2011 az 2019)
	 * p - https://ourworldindata.org/covid-government-stringency-index
	 */
	private static void nactiData() {
		vstupniData = new LinkedHashMap<Integer, TydenniData>();
		
		vstupniData.put(1, new TydenniData(29, 2089, 17489.0/9, 269.45));
		vstupniData.put(2, new TydenniData(56, 2220, 17712.0/9, 272.23));
		vstupniData.put(3, new TydenniData(99, 2392, 18025.0/9, 272.23));
		vstupniData.put(4, new TydenniData(146, 2381, 18536.0/9, 272.23));
		vstupniData.put(5, new TydenniData(301, 2609, 18694.0/9, 337.05));
		vstupniData.put(6, new TydenniData(496, 2946, 18537.0/9, 337.05));
		vstupniData.put(7, new TydenniData(899, 3647, 18624.0/9, 414.81));
		vstupniData.put(8, new TydenniData(1359, 4226, 18337.0/9, 512.05));
		vstupniData.put(9, new TydenniData(1534, 4238, 18137.0/9, 512.05));
		vstupniData.put(10, new TydenniData(1277, 3852, 18339.0/9, 512.05));
		vstupniData.put(11, new TydenniData(976, 3424, 18405.0/9, 493.5));
		vstupniData.put(12, new TydenniData(868, 3169, 18858.0/9, 486.08));
		vstupniData.put(13, new TydenniData(756, 3100, 19089.0/9, 456.48));
		vstupniData.put(14, new TydenniData(697, 3104, 19223.0/9, 434.28));
		vstupniData.put(15, new TydenniData(767, 3242, 19819.0/9, 445.38));
		vstupniData.put(16, new TydenniData(771, 3214, 20066.0/9, 504.63));
		vstupniData.put(17, new TydenniData(987, 3544, 19404.0/9, 512.05));
		vstupniData.put(18, new TydenniData(1228, 3833, 20750.0/9, 512.05));
		vstupniData.put(19, new TydenniData(1164, 3744, 20229.0/9, 512.05));
		vstupniData.put(20, new TydenniData(1041, 3591, 20523.0/9, 534.25));
		vstupniData.put(21, new TydenniData(976, 3443, 20680.0/9, 556.46));
		vstupniData.put(22, new TydenniData(920, 3270, 21080.0/9, 570.36));
		vstupniData.put(23, new TydenniData(946, 3436, 21471.0/9, 562.02));
		vstupniData.put(24, new TydenniData(1091, 3476, 21519.0/9, 525));
		vstupniData.put(25, new TydenniData(1206, 3627, 21551.0/9, 525));
		vstupniData.put(26, new TydenniData(1415, 3916, 21141.0/9, 525));
		vstupniData.put(27, new TydenniData(1545, 4026, 20850.0/9, 570.36));
		vstupniData.put(28, new TydenniData(1421, 3832, 20547.0/9, 570.36));
		vstupniData.put(29, new TydenniData(1202, 3468, 20170.0/9, 570.36));
		vstupniData.put(30, new TydenniData(1033, 3386, 19823.0/9, 570.36));
		vstupniData.put(31, new TydenniData(791, 2946, 19429.0/9, 570.36));
		vstupniData.put(32, new TydenniData(569, 2640, 19116.0/9, 414.82));
		vstupniData.put(33, new TydenniData(420, 2510, 18843.0/9, 395.36));
		vstupniData.put(34, new TydenniData(334, 2472, 18888.0/9, 395.36));
		vstupniData.put(35, new TydenniData(256, 2298, 18139.0/9, 395.36));
		vstupniData.put(36, new TydenniData(167, 2245, 17912.0/9, 395.36));
	}
}
