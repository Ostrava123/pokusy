package umrtnost.data;

import java.util.Formatter;

/**
 * Datova trida pro vstupni data.
 * 
 * @author Vladislav Zidek
 *
 */
public class TydenniData {
	private int covidUmrtnost;			// Zadane zname umrti na/s covid(em)
	private int aktualniUmrtnost;		// Zjistena celkova umrtnost
	private double prumernaUmrtnost;	// Prumerna umrtnost za posledni leta
	private double prisnost;			// Prisnost opatreni
	
	public int getCovidUmrtnost() {
		return covidUmrtnost;
	}

	public double getNadumrtnost() {
		return aktualniUmrtnost - prumernaUmrtnost;
	}

	public double getPrumernaUmrtnost() {
		return prumernaUmrtnost;
	}

	public int getAktualniUmrtnost() {
		return aktualniUmrtnost;
	}
	
	public double getPrisnost() {
		return prisnost;
	}
	
	/**
	 * Konstruktor
	 * 
	 * @param cu Umrti na/s covid(em)
	 * @param au Celkova umrti
	 * @param pu Prumerne umrti
	 * @param p Prisnost
	 */
	public TydenniData(int cu, int au, double pu, double p) {
		super();
		covidUmrtnost = cu;
		aktualniUmrtnost = au;
		prumernaUmrtnost = pu;
		prisnost = p;
	}
	
	public String toString() {
		Formatter f = new Formatter();
		String s = f.format("covidUmrtnost: %d , aktualniUmrtnost: %d, prumernaUmrtnost: %f, prisnost: %f",
				covidUmrtnost, aktualniUmrtnost, prumernaUmrtnost, prisnost).toString();
		f.close();
		return s;
	}
}
