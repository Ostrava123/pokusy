package umrtnost.data;

import java.util.Formatter;

/**
 * Datova trida pro konecne i prubezne vysledky.
 * 
 * @author Vladislav Zidek
 *
 */
public class Vysledek{
	private double odhadCovid;		// Odhad zemrelych na/s covidem().
	private double odhadOpatreni;	// Odhad zemrelych v souvislosti s opatření.
	
	/**
	 * Zbytek zemrelych. Tato hodnota je minimalizovana (presneji jeji druha mocnina).
	 * Absolutni hodnota z rozdilu mezi nadumrtnosti a souctem odhadu zemrelych.
	 * Tedy rezidium = |nadumrtnost - (odhadCovid + odhadOpatreni)|  
	 */
	private double rezidium;		
	
	public double getOdhadCovid() {
		return odhadCovid;
	}

	public double getOdhadOpatreni() {
		return odhadOpatreni;
	}
	
	public double getRezidium() {
		return rezidium;
	}
	
	/**
	 * Konstruktor
	 * 
	 * @param c Odhad umrtnosti na/s covid(em)
	 * @param o Odhad umrtnosti v souvislosti s opatrenimi
	 * @param r Rezidium
	 */
	public Vysledek(double c, double o, double r) {
		odhadCovid = c;
		odhadOpatreni = o;
		rezidium = r;
	}
	
	public String toString() {
		Formatter f = new Formatter();
		String s = f.format("odhadCovid: %f , odhadOpatreni: %f, rezidium: %f", odhadCovid, odhadOpatreni, rezidium).toString();
		f.close();
		return s;
	}
}
